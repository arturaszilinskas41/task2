<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

abstract class Filters
{
    protected $builder;
    protected $request;
    protected $filters = [];
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    public function apply($builder) : Builder
    {
        $this->builder = $builder;

        foreach ($this->request->only($this->filters) as $filter => $value) {
            $value = utf8_decode(urldecode($value));
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $builder;
    }
}