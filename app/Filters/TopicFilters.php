<?php

namespace App\Filters;

use App\User;

class TopicFilters extends Filters
{   
    protected $filters = ['username'];
    
    public function username($username)
    {
        $user = User::where('name', $username)->firstOrFail();
        return $this->builder->where('user_id', $user->id);
    }
}