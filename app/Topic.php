<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $guarded = [];
    protected $with = ['user'];
    
    protected static function boot()
    {
        parent::boot();
        
        static::addGlobalScope('commentsCount', function($builder){
            $builder->withCount('comments');
        });
        
        static::deleting(function($topic)
        {
            $topic->comments()->delete();
        });
    }
    
    public function path()
    {
        return '/topics/' . $this->id;
    }
    
    public function comments()
    {
        return $this->hasMany(Comment::class)->withCount('favorites');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function addComment($comment)
    {
        $this->comments()->create($comment);
    }
    
    public function scopeFilter($builder, $filters)
    {
        return $filters->apply($builder);
    }        
    
}
