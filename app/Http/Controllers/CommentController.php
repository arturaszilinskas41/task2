<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Topic $topic, Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);
        
        $topic->addComment([
            'body' => request('body'),
            'user_id' => Auth::id()
        ]);
        
        return back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Topic  $comments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('forceDelete', $comment);
        
        $comment->delete();
        if(request()->wantsJson())
            return response([], 204);
        
        return back();
    }
    
}
