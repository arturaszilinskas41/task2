<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class FavoriteController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, Comment $comment)
    {
        $comment->favorite(); 
        return back();
    }
}
