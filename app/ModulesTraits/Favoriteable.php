<?php

namespace App\ModulesTraits;

use App\Favorite;
use Illuminate\Support\Facades\Auth;

trait Favoriteable
{
    protected static function boot()
    {
        parent::boot();
        
        static::deleting(function($topic)
        {
            $topic->favorites()->delete();
        });
    }
    
    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorited');
    }
    
    public function favorite()
    {
        if (!$this->favorites()->where(['user_id' => Auth::id()])->exists()) {
            return $this->favorites()->create(['user_id' => Auth::id()]);
        }
    }
    
    public function isFavorited()
    {
        return $this->favorites()->where('user_id', Auth::id())->exists();
    }
}