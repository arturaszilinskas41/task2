<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModulesTraits\Favoriteable;
use App\Topic;

class Comment extends Model
{
    use Favoriteable;
    
    protected $guarded = [];
    protected $with = ['user', 'favorites'];
    
    public function path()
    {
        return '/comments/' . $this->id;
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }
    
}
