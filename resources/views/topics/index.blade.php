@extends('layouts.app')

@section('content')

<div class="container">
    
    <div class="row">
        
        <div class="panel panel-default">
            
            <div class="panel-heading">
                
                Topics
                
            </div>
            
            <div class="panel-body">
                
                @foreach($topics as $topic)
                
                    <article>

                        <h4>
                            
                            <a href="/topics?username={{$topic->user->name}}">{{ $topic->user->name }}</a> posted:
                            
                            <a href="{{$topic->path()}}">{{$topic->title}}</a>
                            
                        </h4>
                        
                        <div class="body">
                            
                            <div>{{$topic->body}}</div>
                            
                            <div>Created at {{$topic->created_at}}</div>
                            
                            <div>Has {{$topic->comments_count}} {{ str_plural('comment', $topic->comments_count) }}</div>
                            
                        </div>
                        
                    </article>
                    
                @endforeach
                
                {{$topics->links()}}
                
            </div>
            
        </div>
        
    </div>
    
</div>
@endsection