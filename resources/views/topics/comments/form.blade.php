            @if(auth()->check())

                <form method="post" action="{{$topic->path()}} . /comments">
                    
                    <div class="form-group">
                        {{csrf_field()}}
                        
                        <textarea name="body" id="body" class="form-control" rows="3" required>
                        </textarea>
                        
                        <button type="submit" class="btn btn-dark">
                            Post a comment
                        </button>
                        
                    </div>
                    
                </form>

                @include('errors.form_errors')
            
            @else

            <p>
                
                Please <a href="{{route('login')}}">login</a> if u want to comment this topic
                
            </p>

            @endif