<div class="panel panel-default">
 
    <div class="panel-body">
        <div class="level">
            <h5 class="flex">
                <a href="#">{{$comment->user->name}}</a> said {{$comment->created_at}}
            </h5>
 
            <div class="row">
                <form method="POST" action="/comments/{{$comment->id}}/favorites">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-dark {{$comment->isFavorited() ? 'disabled' : '' }}" {{$comment->isFavorited() ? 'disabled' : '' }}>
                        {{$comment->favorites_count}} {{str_plural('Favorite', $comment->favorites_count)}}
                    </button>
                </form>
                @can('forceDelete', $comment)
                    <form action="{{$comment->path()}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-dark">Delete Comment</button>
                    </form>
                @endcan
            </div>
        </div>
    </div>
 
    <div class="panel-body">
        {{ $comment->body }}
    </div>
</div>