@extends('layouts.app')

@section('content')

<div class="container">
    
    <div class="row">
        
        <div class="panel panel-default">
            
            <div class="panel-heading">
                
                Create a topic:
                
            </div>
            
            <div class="panel-body">
                
                <form method="post" action="/topics">
                    {{csrf_field()}}
                    
                    <div class="form-froup">
                        <label>
                            Title:
                        </label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="body">Body:</label>
                        <textarea name="body" id="body" class="form-control" rows="10" required>{{ old('body') }}</textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-dark">
                        Post a topic
                    </button>
                    
                </form>
                
                @include('errors.form_errors')
                    
            </div>
            
        </div>
        
    </div>
    
</div>
@endsection