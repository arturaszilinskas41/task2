@extends('layouts.app')

@section('content')

<div class="container">
    
    <div class="row">
        
        <div class="panel panel-default">
            
            <div class="panel-heading">
                
                {{$topic->title}}
                
            </div>
            
            <div class="panel-body">

                <div>
                
                    {{$topic->body}}
                
                </div>
                    
            </div>
            
        </div>
        
    </div>
    
    <div class="row">
            
        <div class="panel panel-default">
         
            @foreach($comments as $comment)
                
                @include("topics.comments.comment")
                
            @endforeach
            
            {{$comments->links()}}
                
        </div>
            
    </div>
    
     <div class="row">
    
        <div class="panel panel-default">
            
            @include("topics.comments.form")
            
        </div>
    
    </div>
    
    @can('forceDelete', $topic)
        <form action="{{$topic->path()}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button class="btn btn-dark">Delete Topic</button>
        </form>
    @endcan
    
</div>
@endsection