<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::resource('topics', 'TopicController');
Route::post('topics/{topic}/comments', 'CommentController@store');
Route::post('comments/{comment}/favorites', 'FavoriteController@store');
Route::delete('comments/{comment}', 'CommentController@destroy');


//Route::get('/home', 'HomeController@index')->name('home');
