<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TopicTest extends TestCase
{
    use DatabaseMigrations;
    
    public function setUp() : void
    {
        parent::setUp();
        
        $this->topic = factory('App\Topic')->create();
    }
    
    public function testTopicHasComment()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->topic->comments);
    }
    
    public function testTopicHasAUser()
    {
        $this->assertInstanceOf('App\User', $this->topic->user);
    }
    
    public function testTopicCanAddComment()
    {
        $this->topic->AddComment([
            'body' => 'asdf_123',
            'user_id' => 1
        ]);
        
        $this->assertCount(1, $this->topic->comments);
    }
}
