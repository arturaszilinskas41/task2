<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testCommentHasAUserThatCreatedThisComment()
    {
        $comment = factory('App\Comment')->create();
        
        $this->assertInstanceOf('App\User', $comment->user);
    }
}
