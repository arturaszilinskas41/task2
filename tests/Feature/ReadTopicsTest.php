<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Auth;

class ReadTopicsTest extends TestCase
{
    use DatabaseMigrations;
    
    public function setUp() : void
    {
        parent::setUp();
        $this->topic = factory('App\Topic')->create();
    }
    
    public function testUserCanSeeTopics()
    {
        $response = $this->get('/topics');
        $response->assertSee($this->topic->title);
        $response->assertStatus(200);
    }
    
    public function testUserCanSeeSingeTopic()
    {
        $response = $this->get('/topics/' . $this->topic->id);
        $response->assertSee($this->topic->title);
        $response->assertStatus(200);
    }
    
    public function testUserCanSeeCommentsAssociatedWithTopic()
    {
        $comment = factory('App\Comment')->create(['topic_id' => $this->topic->id]);
        $response = $this->get('/topics/' . $this->topic->id);
        $response->assertSee($comment->body);
        $response->assertStatus(200);
    }
    
    public function testCanFilterByUsername()
    {
        $this->actingAs(factory('App\User')->create(['name' => 'ASDF']));
        $topicByASDF = factory('App\Topic')->create(['user_id' => Auth::id()]);
        $topicNotByASDF = factory('App\Topic')->create();
        
        $response = $this->get('/topics?username=ASDF');
        $response->assertSee($topicByASDF->title);
        $response->assertDontSee($topicNotByASDF->title);
        
    }
}
