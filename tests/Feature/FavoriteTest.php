<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FavoriteTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testLoggedInUserCanFavoriteAComment()
    {
        $this->actingAs(factory('App\User')->create());
        $comment = factory('App\Comment')->create();
        $this->post('/comments/' . $comment->id . '/favorites');
        $this->assertCount(1, $comment->favorites);
    }
    
    public function testUnauthenticatedUserCantFavoriteAComment()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $comment = factory('App\Comment')->create();
        $this->post('/comments/' . $comment->id . '/favorites');
    }
    
    public function testUnauthenticatedUserCantFavoriteACommentOnce()
    {
        $this->actingAs(factory('App\User')->create());
        $comment = factory('App\Comment')->create();
        $this->post('/comments/' . $comment->id . '/favorites');
        $this->post('/comments/' . $comment->id . '/favorites');
        $this->assertCount(1, $comment->favorites);
    }
}
