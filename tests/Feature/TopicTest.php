<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TopicTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testLoggedInUserCanMakeNewTopics()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $topic = factory('App\Topic')->make();
        $response = $this->post('/topics', $topic->toArray());
        $response = $this->get($response->headers->get('Location'));
        $response->assertSee($topic->title);
        $response->assertSee($topic->body);
    }
    
    public function testUnauthenticatedUsersCantMakeNewTopic()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $topic = factory('App\Topic')->make();
        $this->post('topics', $topic->toArray());
    }
    
    public function testUnauthenticatedUserCantSeeTopicForm()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $response = $this->get('/topics/create');
    }
    
    public function testUserCanDeleteHisSelfTopic()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $topic = factory('App\Topic')->create(['user_id'=>$user->id]);
        $comment = factory('App\Comment')->create(['topic_id' => $topic->id, 'user_id'=>$user->id]);
        $response = $this->json('DELETE', $topic->path());
        $this->assertDatabaseMissing('topics', ['id' => $topic->id]);
        $this->assertDatabaseMissing('comments', ['id' => $comment->id]);
        $response->assertStatus(204);
    }        
    
    public function testUnauthenticatedUserCantDeleteTopic()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $topic = factory('App\Topic')->create();
        $this->json('DELETE', $topic->path());
    }
    
    public function testCanOnlyDeleteUrSelfCreatedTopics()
    {
        $this->expectException('Illuminate\Auth\Access\AuthorizationException');
        $my_topic = factory('App\Topic')->create();
        $other_user = factory('App\User')->create();
        $this->actingAs($other_user);
        $response = $this->json('DELETE', $my_topic->path());
    }
    
    public function testTopicValidateTitleNull()
    {
        $this->validateTopic(['title'=>null]);
    }
    
    public function testTopicValidateBodyNull()
    {
        $this->validateTopic(['body'=>null]);
    }
    
    private function validateTopic(array $rules)
    {
        $this->expectException('Illuminate\Validation\ValidationException');
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $topic = factory('App\Topic')->make($rules);
        $this->post('/topics', $topic->toArray());
    }
}
