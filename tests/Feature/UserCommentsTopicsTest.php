<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserCommentsTopicsTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testAnAuthenticatedUserCanCommentTopic()
    {
        $user = factory('App\User')->create();
        $this->be($user);
        $topic = factory('App\Topic')->create();
        $comment = factory('App\Comment')->make();
        $this->post('topics/' . $topic->id . '/comments' , $comment->toArray());
        $this->get($topic->path())->assertSee($comment->body);
    }
    
    public function testUnauthenticatedUserCanNotCommentTopic()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->post('topics/1/comments', []);
    }
    
    public function testUserCantDeleteSomeoneElseComment()
    {
        $this->expectException('Illuminate\Auth\Access\AuthorizationException');
        $another_user = factory('App\User')->create();
        $this->actingAs($another_user);
        $topic = factory('App\Topic')->create();
        $comment = factory('App\Comment')->create();
        $this->json('DELETE', '/comments/' . $comment->id);
    }
    
    public function testAnAuthenticatedUserCanDeleteHisSelfComment()
    {
        $user = factory('App\User')->create();
        $this->actingAs($user);
        $topic = factory('App\Topic')->create();
        $comment = factory('App\Comment')->create(['user_id' => $user->id]);
        $comment->favorite();
        $this->assertDatabaseHas('favorites', ['user_id' => $user->id, 'favorited_id' => $comment->id, 'favorited_type' => 'App\Comment' ]);
        
        $response = $this->json('DELETE', '/comments/' . $comment->id);
                
        $this->assertDatabaseMissing('comments', ['id' => $comment->id]);
        $this->assertDatabaseMissing('favorites', ['user_id' => $user->id, 'favorited_id' => $comment->id, 'favorited_type' => 'App\Comment' ]);
        $response->assertStatus(204);
    }
    
    public function testTopicValidateBodyNull()
    {
        $this->validateComment(['body'=>null]);
    }
    
    private function validateComment(array $rules)
    {
        $this->expectException('Illuminate\Validation\ValidationException');
        $user = factory('App\User')->create();
        $this->actingAs($user);
        
        $topic = factory('App\Topic')->create();
        $comment = factory('App\Comment')->make($rules);
        
        $this->post($topic->path() . '/comments', $comment->toArray());
    }
}
